import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import { routes } from './routes'

//define the vue router
Vue.use(VueRouter)

//import the created routes and set the mode to history to remove the # at the end of the url
const router = new VueRouter({
  routes,
  mode: 'history'
})

//define app.vue as the parent component
new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
