//importing each of the components
import Home from './components/Home.vue'
import AboutUs from './components/AboutUs.vue'
import MeetTheTeam from './components/MeetTheTeam.vue'
import Services from './components/Services.vue'
import Gallery from './components/Gallery.vue'
import ContactUs from './components/ContactUs.vue'

//route component outlining what component to render for each of the routes
export const routes = [
  {
    path: '', component: Home
  },
  {
    path: '/about-us', component: AboutUs
  },
  {
    path: '/about-us/meet-the-team', component: MeetTheTeam
  },
  {
    path: '/services', component: Services
  },
  {
    path: '/gallery', component: Gallery
  },
  {
    path: '/contact-us', component: ContactUs
  }
]